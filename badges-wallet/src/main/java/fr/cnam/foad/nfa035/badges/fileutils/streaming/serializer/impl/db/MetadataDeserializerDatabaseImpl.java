package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class MetadataDeserializerDatabaseImpl implements MetadataDeserializer {

    private static final Logger LOG = LogManager.getLogger(MetadataDeserializerDatabaseImpl.class);

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public Set<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException {

        BufferedReader br = media.getEncodedImageReader(false);
        return br.lines()
                .map(
                    l-> l.split(";")
                )
                .filter(s -> s.length == 4)
                .map(
                        s -> new DigitalBadgeMetadata(Integer.parseInt(s[0]),Long.parseLong(s[1]),Long.parseLong(s[2]))
                )
                .collect(Collectors.toSet());
    }

    /**
     * Peremet de lire la dernière ligne d'un fichier en accès direct
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        for(long seek = length; seek >= 0; --seek){
            file.seek(seek);
            char c = (char)file.read();
            if(c != '\n' || seek == 1)
            {
                builder.append(c);
            }
            else{
                builder = builder.reverse();
                break;
            }
        }
        return builder.toString();
    }

}
